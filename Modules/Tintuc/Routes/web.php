<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Tintuc\Http\Middleware\AuthCustomMiddleware;

Route::prefix('admin')->group(function() {
    Route::get('/', 'Admin\AuthController@redirect')->name('admin.auth.redirect')->middleware(AuthCustomMiddleware::class);
    Route::get('/login', 'Admin\AuthController@index')->name('admin.auth.index');
    Route::post('/login', 'Admin\AuthController@checkLogin')->name('admin.auth.login');
    Route::get('/logout', 'Admin\AuthController@logout')->name('admin.auth.logout');
    Route::group(array("prefix"=> "tintuc", "middleware" => AuthCustomMiddleware::class), function() {
        Route::resources([
            '/category' => 'Admin\CategoryController',
            '/user' => 'Admin\UserController',
            '/news' => 'Admin\NewsController'
        ]);
    });
});
Route::get('/', 'Front\NewsController@index')->name('front.news.index');
Route::get('/news/{id}', 'Front\NewsController@show')->name('front.news.show');
