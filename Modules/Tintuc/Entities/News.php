<?php

namespace Modules\Tintuc\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Modules\Tintuc\Events\NewsWasUpdated;

class News extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $dispatchesEvents = [
        'saved' => NewsWasUpdated::class,
    ];

    protected $fillable = [
        'id', 'title', 'content', 'slug', 'created_by', 'cate_id'
    ];

    protected $table = 'new';
    
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class, 'cate_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
