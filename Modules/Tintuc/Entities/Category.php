<?php

namespace Modules\Tintuc\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $fillable = [
        'name', 'slug', 'status'
    ];

    protected $table = 'category';

    public $timestamps = false;
    
    public function news()
    {
        return $this->hasMany(News::class, 'cate_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    
}
