<?php

namespace Modules\Tintuc\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    
    protected $fillable = [
        'username', 'password', 'full_name'
    ];

    protected $table = 'users';

    public $timestamps = false;
    
    public function news()
    {
        return $this->hasMany(News::class, 'foreign_key', 'created_by');
    }

}
