<?php

namespace Modules\Tintuc\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Tintuc\Entities\User;
use Modules\Tintuc\Entities\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Cviebrock\EloquentSluggable\Services\SlugService;

class TintucDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        User::insert([
            [
                'full_name' => 'Nguyen Van A',
                'username' => 'usera',
                'password' => Hash::make('123456'),
                'remember_token' => Str::random(10),
        ],
            [
                'full_name' => 'Nguyen Van B',
                'username' => 'userb',
                'password' => Hash::make('123456'),
                'remember_token' => Str::random(10),
            ]
        ]);
        Category::insert([
            [
                'name' => 'Xa Hoi',
                'slug' => SlugService::createSlug(Category::class, 'slug', 'Xa hoi'),
                'status' => 1,
        ],
            [
                'name' => 'Chinh tri',
                'slug' => SlugService::createSlug(Category::class, 'slug', 'Chinh tri'),
                'status' => 1,
        ]
        ]);
    }
}
