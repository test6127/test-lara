<?php

namespace Modules\Tintuc\Listeners;

use Modules\Tintuc\Events\NewsWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddHotToNewPost
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param NewsWasUpdated $event
     * @return void
     */
    public function handle(NewsWasUpdated $event)
    {
        
    }
}
