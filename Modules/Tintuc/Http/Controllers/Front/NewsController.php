<?php

namespace Modules\Tintuc\Http\Controllers\front;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Tintuc\Entities\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $news = News::with('category')->with('user')->paginate(5);
        return view('tintuc::front.pages.news.index', compact('news'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($slug)
    {
        $news = News::findBySlug($slug);
        return view('tintuc::front.pages.news.detailt', compact('news'));
    }

}
