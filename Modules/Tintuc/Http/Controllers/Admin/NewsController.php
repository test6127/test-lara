<?php

namespace Modules\Tintuc\Http\Controllers\admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Tintuc\Entities\News;
use Modules\Tintuc\Entities\Category;
use Modules\Tintuc\Http\Requests\NewsRequest;
use Session;
use Cviebrock\EloquentSluggable\Services\SlugService;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $news = News::with('category')->with('user')->paginate(5);
        return view('tintuc::admin.pages.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categories = Category::all();
        return view('tintuc::admin.pages.news.edit', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(NewsRequest $request)
    {
        $news = new News;
        $news->title = $request->title;
        $news->content = $request->content;
        $news->cate_id = $request->cate_id;
        $news->hot = $request->has('hot') ? 1 : 0;
        $news->slug = SlugService::createSlug(News::class, 'slug', $request->get('title'));
        $news->created_by = Auth::user()->id;
        $news->save();
        Session::flash('success', 'Successfully added');
        return redirect()->route('news.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('tintuc::admin.pages.news.edit');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $news = News::find($id);
        $categories = Category::all();
        return view('tintuc::admin.pages.news.edit', compact('news', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(NewsRequest $request, $id)
    {
        $news = News::find($id);
        $news->title = $request->title;
        $news->content = $request->content;
        $news->cate_id = $request->cate_id;
        $news->hot = $request->has('hot') ? 1 : 0;
        $news->save();
        Session::flash('success', 'Update successful');
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        Session::flash('success', 'News deleted successfully');
        return redirect()->route('news.index');
    }
}
