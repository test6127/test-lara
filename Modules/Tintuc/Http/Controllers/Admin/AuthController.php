<?php

namespace Modules\Tintuc\Http\Controllers\admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Tintuc\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('tintuc::admin.pages.auth.login');
    }

    public function checkLogin(LoginRequest $request){
        $info = [
            'username' => $request->username,
            'password' => $request->password,
        ];
        if(Auth::attempt($info)){
            return redirect()->route('news.index');
        }else{
            return back()->with('error', 'Login failed');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.auth.index');
    }

    public function redirect()
    {
        return redirect()->route('news.index');
    }

}
