@extends('tintuc::front.layouts.master')

@section('content')
    <div class="content">
        <div class="block block-rounded">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center">@if($news->hot)<span class="badge badge-danger">HOT</span>@endif {{$news->title}}</h3>
                    <small class="p-2 font-weight-bold">Category: {{$news->category->name}}</small><br>
                    <small class="p-2 font-weight-bold font-italic">Date created: {{$news->created_at}}</small><br>
                    <small class="p-2 font-weight-bold font-italic">Created by: {{$news->user->full_name}}</small>
                </div>
                <div class="col-lg-12 p-4">
                    {!! $news->content !!}
                </div>
                <div class="col-lg-12 p-4">
                    <a href="{{route('front.news.index')}}" class="btn btn-primary">List news</a>
                </div>
            </div>
        </div>
    </div>
@endsection