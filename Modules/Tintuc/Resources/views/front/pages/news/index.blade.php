@extends('tintuc::front.layouts.master')

@section('content')
    <div class="content">
        <div class="block block-rounded">
            <div class="block-header">
                <h3 class="block-title">News</h3>
            </div>
            @if($news->isEmpty())
                <h3 class="text-center">No news</h3>
            @else
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th style="width: 15%;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $key => $new)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="font-w600">
                                <a href="{{route('front.news.show', ['id' => $new->slug])}}">@if($new->hot)<span class="badge badge-danger">HOT</span>@endif {{$new->title}}</a>
                            </td>
                            <th>{{$new->category->name}}</th>
                            <td>
                                <a href="{{route('front.news.show', ['id' => $new->slug])}}" class="btn btn-sm btn-primary">View</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-lg-12">
                    <div class="news_paginate">
                        {{$news->links()}}
                    </div>
                </div>

            </div>
            @endif
        </div>
    </div>

@endsection