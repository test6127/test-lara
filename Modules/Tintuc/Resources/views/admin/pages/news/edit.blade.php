@extends('tintuc::admin.layouts.master')

@section('js_after')
    <script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection

@section('content')
    <div class="content">
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $err)
                    <div class="">{{$err}}</div>
                @endforeach
            </div>
        @endif
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <form method="post"
                      action="@if(isset($news)){{route('news.update', ['news' => $news->id])}}@else{{route('news.store')}}@endif">
                    @if(isset($news))
                        @method('PUT')
                    @endif
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" placeholder="Enter title" name="title"
                               value="@if(old('title')) {{old('title')}} @elseif(isset($news)) {{$news->title}} @endif"
                               required>
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select class="custom-select mr-sm-2" name="cate_id" id="inlineFormCustomSelect">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                        @if(isset($news) && $news->cate_id == $category->id || old('cate_id')) selected @endif>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Content</label>
                        <textarea class="form-control" id="content"
                                  name="content">@if(old('content')) {!! old('content') !!} @elseif(isset($news)) {!! $news->content !!} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" name="hot"
                                   @if(isset($news) && $news->hot) checked @endif>
                            <label class="form-check-label" for="defaultCheck1">
                                HOT
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="{{route('news.index')}}" class="btn btn-primary">Back</a>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection