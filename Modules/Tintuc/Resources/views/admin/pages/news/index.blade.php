@extends('tintuc::admin.layouts.master')

@section('content')
<div class="content">
    @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
    @endif
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">News</h3>
        </div>
        <div class="block-content block-content-full">
            <a href="{{route('news.create')}}" class="btn btn-primary btn-add-news">Add news</a>
            @if($news->isEmpty())
                <h3 class="text-center">No news</h3>
            @else
            <table class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Created by</th>
                        <th class="d-none d-sm-table-cell" >Date created</th>
                        <th class="d-none d-sm-table-cell">Date updated</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($news as $key => $new)
                    <tr>
                        <td class="text-center">{{$key+1}}</td>
                        <td class="font-w600">
                            <a target="_blank" href="{{route('front.news.show', ['id' => $new->slug])}}">@if($new->hot)<span class="badge badge-danger">HOT</span>@endif {{$new->title}}</a>
                        </td>
                        <th>{{$new->category->name}}</th>
                        <th>{{$new->user->full_name}}</th>
                        <td class="d-none d-sm-table-cell">
                            {{$new->created_at}}
                        </td>
                        <td class="d-none d-sm-table-cell">
                            {{$new->updated_at}}
                        </td>
                        <td>
                            <a href="{{route('news.edit', ['news' => $new->id])}}" class="btn btn-sm btn-primary" style="float:left;">Edit</a>
                            <form method="post" action="{{route('news.destroy', ['news' => $new->id])}}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger btn-delete-news" onclick="return confirm('Are you sure?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-12">
                <div class="news_paginate">
                    {{$news->links()}}
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection